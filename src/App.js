import React, { Component } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import HomePage from "./pages/Home";
import LoginPage from "./pages/LoginPage";
import AboutPage from "./pages/AboutPage";
import Quiz from "./components/Quiz";
import Loader from "./components/Loader";
import { PrivateRoute } from "./utils/privateRoute";
import FirebaseContext from "./context/firebaseContext";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: null,
    };
  }

  componentDidMount() {
    const { auth, setUserUid } = this.context;

    auth.onAuthStateChanged((user) => {
      if (user) {
        setUserUid(user.uid);
        this.setState({ user });
      } else {
        setUserUid(null);
        this.setState({ user: false });
      }
    });
  }

  render() {
    const { user } = this.state;

    if (user === null) {
      return (
        <div>
          <Loader />
        </div>
      );
    }

    return (
      <>
        <BrowserRouter>
          <Switch>
            <Route path='/login' component={LoginPage} />
            <PrivateRoute path='/' exact component={HomePage} />
            <PrivateRoute path='/home' exact component={HomePage} />
            <PrivateRoute path='/about' component={AboutPage} />
            <PrivateRoute path='/quiz' component={Quiz} />
            <Redirect to='/' />
          </Switch>
        </BrowserRouter>
      </>
    );
  }
}

App.contextType = FirebaseContext;

export default App;
