import React, { Component } from "react";
import { nanoid } from "nanoid";

import HeaderBlock from "../../components/HeaderBlock";
import Header from "../../components/Header";
import Paragraph from "../../components/Paragraph";
import Cards from "../../components/Cards";
import Section from "../../components/Section";
import TextLine from "../../components/TextLine";

import Gandam from "./img/gandam.jpg";

import FirebaseContext from "../../context/firebaseContext";

class HomePage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      words: [],
    };
  }

  deleteItemHandler = (id) => {
    const { updateCards } = this.context;
    const { words } = this.state;
    const newArr = words.filter((item) => item.id !== id);

    updateCards(newArr);
  };

  addItemHandler = (eng, jpn) => {
    const { updateCards } = this.context;
    const { words } = this.state;
    const newArr = [
      ...words,
      {
        eng,
        jpn,
        id: nanoid(),
      },
    ];

    updateCards(newArr);
  };

  componentDidMount() {
    const { getUserCardsRef } = this.context;

    getUserCardsRef().on("value", (res) =>
      this.setState({
        words: res.val() || [],
      })
    );
  }

  logoutHandler = () => {
    const { logOut } = this.context;
    const { history } = this.props;
    logOut();
    localStorage.removeItem("user");
    history.push("/");
  };

  render() {
    const { words } = this.state;

    return (
      <>
        <HeaderBlock>
          <Section dark>
            <Header>Хочешь читать мангу и смотреть аниме в оригинале?</Header>
            <Paragraph>
              Изучай японские слова и иероглифы по карточкам. Тренируй память,
              разминай мозг!
            </Paragraph>
          </Section>
          <Section dark>
            <img src={Gandam} alt='gandam' />
          </Section>
        </HeaderBlock>
        <TextLine />
        <Section fullWidth>
          <Cards
            items={words}
            onDeletedItem={this.deleteItemHandler}
            onAddItem={this.addItemHandler}
            lessenNum='Первый урок.'
            lessenDescr='Переворачивай карточки чтобы узначть значения слов и отмечай те, которые уже запомнил.'
          />
        </Section>
        <TextLine />
      </>
    );
  }
}

HomePage.contextType = FirebaseContext;

export default HomePage;
