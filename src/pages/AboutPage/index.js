import React from "react";
import s from "./About.module.scss";
import { Link } from "react-router-dom";
import doraemonPic from "./img/doraemon.png";

function AboutPage() {
  return (
    <div className={s.about}>
      <div className={s.aboutInner}>
        <Link to='/'>Назад</Link>
        <h2 className={s.aboutTitle}>Немного о нас</h2>
        <p className={s.aboutText}>
          Скромное приложение, которое поможет вам изучить японскую азбуку и
          иероглифы.
        </p>
        <img className={s.aboutPic} src={doraemonPic} alt='doraemon cat' />
      </div>
    </div>
  );
}

export default AboutPage;
