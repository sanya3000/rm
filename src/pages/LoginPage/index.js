import React, { Component } from "react";
import s from "./LoginPage.module.scss";
import "antd/dist/antd.css";

import { Layout, Form, Input, Button } from "antd";

import FirebaseContext from "../../context/firebaseContext";

const { Content } = Layout;

class LoginPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: null,
      error: null,
    };
  }

  componentDidMount() {
    const { auth, setUserUid } = this.context;
    auth.onAuthStateChanged((user) => {
      if (user) {
        setUserUid(user.uid);
        localStorage.setItem("user", JSON.stringify(user.uid));
        this.setState({ user });
      } else {
        setUserUid(null);
        localStorage.removeItem("user");
        this.setState({ user: false });
      }
    });
  }

  onFinish = ({ email, password }) => {
    this.setState({ error: null });

    const { signInWithEmail, setUserUid } = this.context;
    const { history } = this.props;

    signInWithEmail(email, password)
      .then((res) => {
        setUserUid(res.user.uid);
        localStorage.setItem("user", JSON.stringify(res.user));
        history.push("/");
      })
      .catch((error) => {
        this.setState({ error: error.message });
      });
  };

  onFinishFailed = (error) => {
    this.setState({ error: error.message });
  };

  renderLoginForm = () => {
    const { error } = this.state;

    const layout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 0 },
    };
    const tailLayout = {
      wrapperCol: { span: 16 },
    };

    return (
      <Form
        {...layout}
        name='basic'
        initialValues={{ remember: true }}
        onFinish={this.onFinish}
        onFinishFailed={this.onFinishFailed}>
        <h3>Вход</h3>
        <Form.Item
          {...tailLayout}
          label='Email'
          name='email'
          rules={[
            { required: true, message: "Это поле обязательно для заполнения" },
          ]}>
          <Input />
        </Form.Item>

        <Form.Item
          {...tailLayout}
          label='Password'
          name='password'
          rules={[
            { required: true, message: "Это поле обязательно для заполнения" },
          ]}>
          <Input.Password />
        </Form.Item>

        <Form.Item>
          <Button
            type='primary'
            htmlType='submit'
            block
            style={{ background: "#ffc5d6", color: "#0d3934", border: "none" }}>
            Войти
          </Button>
        </Form.Item>
        {error && `Ошибка: ${this.state.error}`}
      </Form>
    );
  };

  renderRegistrationForm = () => {
    const { error } = this.state;

    const layout = {
      labelCol: { span: 0 },
      wrapperCol: { span: 0 },
    };
    const tailLayout = {
      wrapperCol: { offset: 8, span: 16 },
    };

    return (
      <Form
        {...layout}
        name='basic'
        initialValues={{ remember: true }}
        onFinish={this.onFinish}
        onFinishFailed={this.onFinishFailed}>
        <h3>Регистрация</h3>
        <Form.Item
          {...tailLayout}
          label='Email'
          name='email'
          rules={[
            { required: true, message: "Укажите имя" },
            { type: "email", message: "Не похоже на email" },
          ]}>
          <Input />
        </Form.Item>

        <Form.Item
          {...tailLayout}
          label='Password'
          name='password'
          rules={[
            { required: true, message: "Придумайте пароль" },
            { min: 6, message: "Пароль должен быть больше 6 символов" },
          ]}>
          <Input.Password />
        </Form.Item>

        <Form.Item
          name='confirm'
          label='Confirm Password'
          dependencies={["password"]}
          hasFeedback
          rules={[
            { required: true, message: "Подтвердите пароль!" },
            { min: 6, message: "Пароль должен быть больше 6 символов" },
            ({ getFieldValue }) => ({
              validator(rule, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }
                return Promise.reject("Пароли не совпадают");
              },
            }),
          ]}>
          <Input.Password />
        </Form.Item>

        <Form.Item>
          <Button
            type='primary'
            htmlType='submit'
            block
            style={{ background: "#ffc5d6", color: "#0d3934", border: "none" }}>
            Зарегистрироваться
          </Button>
        </Form.Item>
        {error && `Ошибка: ${this.state.error}`}
      </Form>
    );
  };

  render() {
    return (
      <Layout>
        <Content>
          <div className={s.page}>
            <div className={s.form}>{this.renderLoginForm()}</div>
          </div>
        </Content>
      </Layout>
    );
  }
}

LoginPage.contextType = FirebaseContext;

export default LoginPage;
