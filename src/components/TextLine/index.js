import React from 'react';
import s from './TextLine.module.scss'

function TextLine() {
    return (
        <div className={s.line}>
            <div className={s.s1}>LEARN ⟶ JAPANISE ⟶ ONLINE ⟶ LEARN ⟶ JAPANISE ⟶ ONLINE ⟶ LEARN ⟶ JAPANISE ⟶ ONLINE ⟶ LEARN ⟵</div>
            <div className={s.s2}>LEARN ⟶ JAPANISE ⟶ ONLINE ⟶ LEARN ⟶ JAPANISE ⟶ ONLINE ⟶ LEARN ⟶ JAPANISE ⟶ ONLINE ⟶ LEARN ⟵</div>
        </div>
    )
}

export default TextLine;
