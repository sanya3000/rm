import React from 'react';
import s from "./Section.module.scss"

const Section = ({dark , fullWidth, children}) => {
    const backgroundColor = dark ? s.sectionLite : s.sectionDark
    const width = fullWidth ? s.sectionFullWidth : null
    const style = [s.section]

    style.push(backgroundColor)
    style.push(width)
    
    return (
        <div className={style.join(' ')}>
            {children}
        </div>
    )
}

export default Section;
