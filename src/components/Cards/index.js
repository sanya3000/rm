import React, { Component } from "react";
import Card from "../Card";
import { getTranslateWord } from "../../services/kanji-dict.js";
import s from "./Cards.module.scss";

class Cards extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: "",
      isBisy: false,
      error: null,
    };
  }

  inputChangeHandler = (e) => {
    this.setState({
      value: e.target.value,
    });
  };

  getWord = async () => {
    const { value } = this.state;
    const word = await getTranslateWord(value);

    this.props.onAddItem(word.meanings[0], word.kanji);

    this.setState(({ value }) => {
      return {
        value: "",
        isBisy: false,
      };
    });
  };

  submitFormHandler = async (e) => {
    e.preventDefault();
    this.setState({ isBisy: true }, this.getWord);
  };

  render() {
    const { value, isBisy } = this.state;
    const { items = [], onDeletedItem, lessenNum, lessenDescr } = this.props;

    return (
      <>
        <div className={s.cards}>
          <div className={s.cards__header}>
            <h2>{lessenNum}</h2>
            <p className={s.cards__text}>{lessenDescr}</p>
          </div>
          {/* <Search
                            
                            placeholder="input search text"
                            enterButton="Search"
                            size="large"
                            loading = {isBisy}
                            onChange={this.inputChangeHandler}
                            onSearch={this.submitFormHandler}
                            value={value}
                        /> */}
          <form className={s.form} onSubmit={this.submitFormHandler}>
            <label>
              {isBisy}
              <input
                type='text'
                placeholder='Введите иероглиф'
                value={value}
                onChange={this.inputChangeHandler}
              />
            </label>
            <button type='submit'>добавить</button>
          </form>
          {items.map(({ jpn, eng, id }) => (
            <Card
              onDeleted={() => onDeletedItem(id)}
              key={id}
              jpn={jpn}
              eng={eng}
            />
          ))}
        </div>
      </>
    );
  }
}

export default Cards;
