import React from "react";
import { Link } from "react-router-dom";
import s from "./HeaderBlock.module.scss";
// import {ReactComponent as ReactLogoSvg} from "../../logo.svg"

const HeaderBlock = ({ hideBackground = false, children }) => {
  const styleCover = hideBackground ? { backgroundImage: "none" } : null;

  return (
    <section className={s.hero} style={styleCover}>
      <Link to='/quiz' className={s.tl}>
        Квиз
      </Link>
      <Link to='/login' className={s.tr}>
        Войти
      </Link>
      <Link to='/' className={s.bl}>
        Домой
      </Link>
      <Link to='/About' className={s.br}>
        О нас
      </Link>
      <div className={s.hero__inner}>{children}</div>
    </section>
  );
};

export default HeaderBlock;
