import React, { Component } from "react";
import { getKanjibyGrade, getTranslateWord } from "../../services/kanji-dict";

import s from "./quiz.module.scss";

class Quiz extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isRuning: false,
      showResult: false,
      kanji: [],
      current: "何",
      answer: "",
      count: 0,
      score: 0,
    };
  }

  componentDidMount() {
    getKanjibyGrade().then((res) => this.setState({ kanji: res }));
  }

  getRandomKanji = () => {
    const { kanji } = this.state;
    const rnd = Math.floor(Math.random() * kanji.length);

    return kanji[rnd];
  };

  startGameHandler = () => {
    this.setState({
      isRuning: true,
      score: 0,
      count: 0,
      showResult: false,
      current: this.getRandomKanji(),
    });
  };

  endGame = () => {
    this.setState({ isRuning: false, showResult: false });
  };

  buttonClickHandler = () => {
    const { count } = this.state;

    this.isCorrect();

    if (count < 10) {
      this.setState({
        current: this.getRandomKanji(),
        count: count + 1,
      });
    } else {
      this.ShowResult();
    }
  };

  ShowResult = () => {
    this.setState({ showResult: true });
  };

  isCorrect = () => {
    const { score, count } = this.state;

    getTranslateWord(this.state.current).then((res) => {
      if (res.meanings[0].toLowerCase() === this.state.answer.toLowerCase()) {
        this.setState({ score: score + 1 });
      } else {
        console.log("endgame" + count + "/" + score);
      }

      this.setState({ answer: "" });
    });
  };

  inputChangeHandler = (e) => {
    this.setState({ answer: e.target.value });
  };

  render() {
    const { answer, isRuning, count, showResult } = this.state;

    return (
      <section className={s.quiz}>
        {isRuning && !showResult ? (
          <div className={s.quizCard}>
            <span className={s.quizStat}>{count} / 10</span>
            <div className={s.quizKanji}>{this.state.current}</div>
            <label>
              <input
                type='text'
                placeholder='Что значит иероглиф?'
                value={answer}
                className={s.quizAnswerInput}
                onChange={this.inputChangeHandler}
              />
            </label>
            <button
              className={s.quizButton}
              onClick={showResult ? this.ShowResult : this.buttonClickHandler}>
              Next
            </button>
          </div>
        ) : (
          <button className={s.startButton} onClick={this.startGameHandler}>
            начать игру
          </button>
        )}
        {showResult && (
          <div className={s.results}>
            <span className={s.resultsText}>
              Вы угадали {this.state.score} из {this.state.count}.
            </span>
          </div>
        )}
      </section>
    );
  }
}

export default Quiz;
