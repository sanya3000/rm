import React from 'react';
import cl from 'classnames'
import s from './Card.module.scss'
import { SmileOutlined, CloseCircleOutlined} from '@ant-design/icons'

class Card extends React.Component {

    state = {
        done: false,
        isRemembered: false
    }

    cardClickHandler = () =>   {
        this.setState(({done}) => {
            return (
                {done: this.state.isRemembered ? true : !done}
            )
        })
    }

    rememberClickHandler = (e) =>   {
        e.stopPropagation()
        this.setState(({isRemembered}) => {
            return (
                {isRemembered: !isRemembered}
            )
        })
    }

     removeClickHandler = (e) =>   {
        e.stopPropagation()
        this.props.onDeleted()
    }
    

    render() {
        const {jpn, eng} = this.props
        const {done, isRemembered} = this.state
 
        return (
            <div className={cl(s.flipCard, {[s.done] :done})}
                onClick={this.cardClickHandler}
                >
                <div className={s.flipCard__inner}>
                    <div className={cl(s.flipCard__front, {[s.remembered]: isRemembered})}>
                        <span className = {s.bar}>
                            <SmileOutlined className={s.icon__remember} onClick={this.rememberClickHandler}/>
                            <CloseCircleOutlined className={s.icon__remove} onClick={this.removeClickHandler} />
                        </span>
                        <p>{jpn}</p>
                    </div>
                    <div className={s.flipCard__back}>
                        {eng}
                    </div>
                </div>
            </div>
        )
    }
}

export default Card;
