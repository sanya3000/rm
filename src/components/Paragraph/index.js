import React from 'react'
import s from './Paragraph.module.scss'

const Header = ({children}) => {
    return (
            <p className={s.text} >{children}</p>
    )
}

export default Header