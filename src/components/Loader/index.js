import React from 'react';
import s from './Loader.module.scss'

function Loader() {
    return (
        <div className={s.loader__wrapper}>
            <div className={s.lds_hourglass}></div>
        </div>
    )
}

export default Loader;
