export const getTranslateWord = async (kanji) => {
  const res = await fetch(`https://kanjiapi.dev/v1/kanji/${kanji}`);
  const body = await res.json();

  return body || [];
};

export const getKanjibyGrade = async (grade) => {
  const res = await fetch(`https://kanjiapi.dev/v1/kanji/grade-1`);
  const body = await res.json();

  return body || [];
};

// export default {getTranslateWord, getKanjibyGrade}
