import * as firebase from "firebase/app";
import "firebase/database";
import "firebase/auth";

const firebaseConfig = {
  apiKey: process.env.REACT_APP_FIREBASE_APIKEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTHDOMAIN,
  databaseURL: process.env.REACT_APP_FIREBASE_DATABASEURL,
  projectId: process.env.REACT_APP_FIREBASE_PROJECTID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGEBUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGINGSENDERID,
  appId: process.env.REACT_APP_FIREBASE_APPID,
};

class Firebase {
  constructor() {
    firebase.initializeApp(firebaseConfig);

    this.auth = firebase.auth();
    this.database = firebase.database();

    this.userUid = null;
  }

  setUserUid = (uid) => (this.userUid = uid);

  signInWithEmail = (email, pass) =>
    this.auth.signInWithEmailAndPassword(email, pass);
  createNewUser = (email, pass) =>
    this.auth.createUserWithEmailAndPassword(email, pass);

  getUserCardsRef = () => this.database.ref(`/cards/${this.userUid}`);

  updateCards = (cards) => this.database.set(cards);

  logOut = () => {
    this.auth.signOut();
  };
}

export default Firebase;
